
//
// Solution to Chapter 10, Page 472, Problem 8 (A, B, C)
// in "A First Book of C++, Fourth Edition"
//
// Written by Gregory McGuire
// web: https://gregory.red
// twitter: https://twitter.com/gm_mcg
// gitlab: gmcg
//

// pardon the garbage formatting, finished this late

#include <iostream> // user i/o

using namespace std;

// Begin class companyEmployee
class companyEmployee {

	private: // Begin private data members.
		int empIDNum; // Employee ID Number
		double empPayRate; // Employee Pay Rate
		int empMaxHours; // The max number of hours an employee can work. (May change this to a double.)
	public:
		companyEmployee(); // todo, may leave empty
		~companyEmployee();
		void viewEmployee(); // displays an employee
		int getID(); // ...
		void editEmployee(int editEmpIDNum, double editEmpPayRate, int editEmpMaxHours); // edits an existing employee, void as it returns nothing
		void addEmployee(int addEmpIDNum, double addEmpPayRate, int addEmpMaxHours); // adds an employee, void as it returns nothing

};

void companyEmployee::viewEmployee() {

	cout << "ID #: " << empIDNum << endl;
	cout << "Pay Rate: " << empPayRate << endl;
	cout << "Hour Cap " << empMaxHours << endl << endl;

}

int companyEmployee::getID() {

	return empIDNum;

}

void companyEmployee::editEmployee(int editEmpIDNum, double editEmpPayRate, int editEmpMaxHours) {

	empIDNum = editEmpIDNum;
	empPayRate = editEmpPayRate;
	empMaxHours = editEmpMaxHours;

	cout << endl << "Employee " << empIDNum << " modified." << endl;

}

void companyEmployee::addEmployee(int addEmpIDNum, double addEmpPayRate, int addEmpMaxHours) {

	empIDNum = addEmpIDNum;
	empPayRate = addEmpPayRate;
	empMaxHours = addEmpMaxHours;

	cout << endl << "Employee " << empIDNum << " created." << endl;

}

companyEmployee::companyEmployee() {

	// init dummy vars
	empIDNum = 0;
	empPayRate = 0.0;
	empMaxHours = 0;
	// may have no effect but hey it is there

}

companyEmployee::~companyEmployee() {

	empIDNum = 0;
	empPayRate = 0;
	empMaxHours = 0;
	cout << endl << "Employee removed from buffer." << endl;

}

int main()
{

	//begin variables
	int userChoice; // for picking an option in the menu required by 8c
	int i = 3; // choosing an employee
	int doesThisEmployeeExist = 0; // well, does it? supposed to be boolean. 0 = no, >0 = yes
	char yesNo;

	//init variables used for adding employees
	int tempEmpIdNum, tempEmpMaxHours;
	double tempEmpPayRate;

	//init example employees
	companyEmployee emp[5]; // can I even use these with arrays?

	emp[0].addEmployee(474378, 11.0, 20);
	emp[1].addEmployee(333678, 12.5, 15);
	emp[2].addEmployee(223339, 10.2, 25);

	// why yes i can

	cout << "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n"; // effectively clear the command window to hide the initialization

	cout << endl << "|| Avalon Telecommunications // Employee Administrator Panel" << endl << endl;
	cout << "---------------------------" << endl;
	cout << "1. Add an employee" << endl;
	cout << "2. Modify employee data" << endl;
	cout << "3. Delete an employee" << endl;
	cout << "4. Exit this menu" << endl;
	cout << "Please enter an option: ";
	cin >> userChoice;

	cout << "\n\n\n\n\n\n\n\n";

	if (userChoice <= 0 || userChoice > 4) {
	
		cout << "Invalid option!" << endl;
		return -1;
	
	}

	switch (userChoice)
	{
	case 1:
		for (i = 3; emp[i].getID() != 0; ++i) {

			// shrug emoji

		}
		cout << "Empty slot found! You are now editing slot number: " << i << endl;
		cout << "Please enter the following information in this order, separated by spaces; ID Number (As a whole number), Pay Rate, and Max Hours" << endl;
		cin >> tempEmpIdNum >> tempEmpPayRate >> tempEmpMaxHours;

		emp[i].addEmployee(tempEmpIdNum, tempEmpPayRate, tempEmpMaxHours);

		cout << "Your employee's info, please make sure it is correct!" << endl;
		emp[i].viewEmployee();
		break;
	case 2:
		cout << "Please enter a number from 0-2: ";
		cin >> userChoice;

		if (userChoice < 0 || userChoice > 2) {
		
			cout << "Invalid option!" << endl;
			return -1;

		}

		cout << "The selected employee's info:" << endl;
		emp[userChoice].viewEmployee();

		cout << "Please enter the following new information in this order, separated by spaces; ID Number (As a whole number), Pay Rate, and Max Hours" << endl;
		cin >> tempEmpIdNum >> tempEmpPayRate >> tempEmpMaxHours;

		emp[userChoice].editEmployee(tempEmpIdNum, tempEmpPayRate, tempEmpMaxHours);
		
		cout << endl;
		cout << "The selected employee's new info:" << endl;
		emp[userChoice].viewEmployee();

		break;
	case 3:
		cout << "Please enter a number from 0-2: ";
		cin >> userChoice;

		if (userChoice < 0 || userChoice > 2) {

			cout << "Invalid option!" << endl;
			return -1;

		}

		cout << "The selected employee's info:" << endl;
		emp[userChoice].viewEmployee();

		cout << "Are you sure you want to delete this employee's info? (y/n): ";
		cin >> yesNo;

		if (yesNo == 'y' || yesNo == 'Y') {
		
			cout << endl << endl;
			emp[userChoice].~companyEmployee();

		}

		cout << endl << "This program may break if you choose to continue!" << endl;

		break;
	case 4:
		// have a nice day :)
		return 0;
	}

	cout << "---------------------------" << endl;
	cout << "1. Add an employee" << endl;
	cout << "2. Modify employee data" << endl;
	cout << "3. Delete an employee" << endl;
	cout << "4. Exit this menu" << endl;
	cout << "Please enter an option: ";
	cin >> userChoice;

	cout << "\n\n\n\n\n\n\n\n";

	if (userChoice <= 0 || userChoice > 4) {

		cout << "Invalid option!" << endl;
		return -1;

	}

	switch (userChoice)
	{
	case 1:
		for (i = 3; emp[i].getID() != 0; ++i) {

			// shrug emoji

		}
		cout << "Empty slot found! You are now editing slot number: " << i << endl;
		cout << "Please enter the following information in this order, separated by spaces; ID Number (As a whole number), Pay Rate, and Max Hours" << endl;
		cin >> tempEmpIdNum >> tempEmpPayRate >> tempEmpMaxHours;

		emp[i].addEmployee(tempEmpIdNum, tempEmpPayRate, tempEmpMaxHours);

		cout << "Your employee's info, please make sure it is correct!" << endl;
		emp[i].viewEmployee();
		break;
	case 2:
		for (i = 3; emp[i].getID() != 0; ++i) {

			// shrug emoji

		}

		i = i - 1;

		cout << "Please enter a number from 0-" << i << ": ";
		cin >> userChoice;

		if (userChoice < 0 || userChoice > i) {

			cout << "Invalid option!" << endl;
			return -1;

		}

		cout << "The selected employee's info:" << endl;
		emp[userChoice].viewEmployee();

		cout << "Please enter the following new information in this order, separated by spaces; ID Number (As a whole number), Pay Rate, and Max Hours" << endl;
		cin >> tempEmpIdNum >> tempEmpPayRate >> tempEmpMaxHours;

		emp[userChoice].editEmployee(tempEmpIdNum, tempEmpPayRate, tempEmpMaxHours);

		cout << endl;
		cout << "The selected employee's new info:" << endl;
		emp[userChoice].viewEmployee();

		break;
	case 3:
		for (i = 3; emp[i].getID() != 0; ++i) {

			// shrug emoji

		}

		i = i - 1;

		cout << "Please enter a number from 0-" << i << ": ";
		cin >> userChoice;

		if (userChoice < 0 || userChoice > i) {

			cout << "Invalid option!" << endl;
			return -1;

		}

		cout << "The selected employee's info:" << endl;
		emp[userChoice].viewEmployee();

		cout << "Are you sure you want to delete this employee's info? (y/n): ";
		cin >> yesNo;

		if (yesNo == 'y' || yesNo == 'Y') {

			cout << endl << endl;
			emp[userChoice].~companyEmployee();

		}

		cout << endl << "This program may break if you choose to continue!" << endl;

		break;
	case 4:
		// have a nice day :)
		return 0;
	}

	cout << "---------------------------" << endl;
	cout << "1. Add an employee" << endl;
	cout << "2. Modify employee data" << endl;
	cout << "3. Delete an employee" << endl;
	cout << "4. Exit this menu" << endl;
	cout << "Please enter an option: ";
	cin >> userChoice;

	cout << "\n\n\n\n\n\n\n\n";

	if (userChoice <= 0 || userChoice > 4) {

		cout << "Invalid option!" << endl;
		return -1;

	}

	switch (userChoice)
	{
	case 1:
		for (i = 3; emp[i].getID() != 0; ++i) {

			// shrug emoji

		}
		cout << "Empty slot found! You are now editing slot number: " << i << endl;
		cout << "Please enter the following information in this order, separated by spaces; ID Number (As a whole number), Pay Rate, and Max Hours" << endl;
		cin >> tempEmpIdNum >> tempEmpPayRate >> tempEmpMaxHours;

		emp[i].addEmployee(tempEmpIdNum, tempEmpPayRate, tempEmpMaxHours);

		cout << "Your employee's info, please make sure it is correct!" << endl;
		emp[i].viewEmployee();
		break;
	case 2:
		for (i = 3; emp[i].getID() != 0; ++i) {

			// shrug emoji

		}

		i = i - 1;

		cout << "Please enter a number from 0-" << i << ": ";
		cin >> userChoice;

		if (userChoice < 0 || userChoice > i) {

			cout << "Invalid option!" << endl;
			return -1;

		}

		cout << "The selected employee's info:" << endl;
		emp[userChoice].viewEmployee();

		cout << "Please enter the following new information in this order, separated by spaces; ID Number (As a whole number), Pay Rate, and Max Hours" << endl;
		cin >> tempEmpIdNum >> tempEmpPayRate >> tempEmpMaxHours;

		emp[userChoice].editEmployee(tempEmpIdNum, tempEmpPayRate, tempEmpMaxHours);

		cout << endl;
		cout << "The selected employee's new info:" << endl;
		emp[userChoice].viewEmployee();

		break;
	case 3:
		for (i = 3; emp[i].getID() != 0; ++i) {

			// shrug emoji

		}

		i = i - 1;

		cout << "Please enter a number from 0-" << i << ": ";
		cin >> userChoice;

		if (userChoice < 0 || userChoice > i) {

			cout << "Invalid option!" << endl;
			return -1;

		}

		cout << "The selected employee's info:" << endl;
		emp[userChoice].viewEmployee();

		cout << "Are you sure you want to delete this employee's info? (y/n): ";
		cin >> yesNo;

		if (yesNo == 'y' || yesNo == 'Y') {

			cout << endl << endl;
			emp[userChoice].~companyEmployee();

		}

		cout << endl << "This program may break if you choose to continue!" << endl;

		break;
	case 4:
		// have a nice day :)
		return 0;
	}
	cout << endl;
    return 0;

}