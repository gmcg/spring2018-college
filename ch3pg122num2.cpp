
// ch3pg122num2.cpp | written by Gregory McGuire


#include <iostream>
#include <iomanip>
using namespace std;


int main()
{
	double length, width, area; // having measures that are not full would be common :v
	
	cout << "Please enter the length of the room: ";
	cin >> length;
	cout << endl << "Please enter the width of the room: ";
	cin >> width;

	area = length * width;

	cout << endl << endl << "The area of the " 
		<< setiosflags(ios::showpoint)
		<< setprecision(4) // make it look nicer
		<< length << " by " << width << " room is: "
		<< setprecision(5)
		<< area << endl;

    return 0;
}
