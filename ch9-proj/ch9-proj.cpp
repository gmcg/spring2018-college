//
// ch9-proj
// An example of file input/output.
// Based off of the material in Chapter 9 of A First Book of C++ (Fourth Edition)
//
// Written By: Gregory McGuire
// Web: https://gregory.red
// Twitter: @gm_mcg
// Gitlab: gmcg
//

// here let me indirectly lift the includes off of page 404 in the book
#include <iostream> // user input/output
#include <fstream> // file I/O
#include <cstdlib> // C Standard Lib
#include <string> // For Strings


using namespace std; // Important

// For the sake of my own sanity, I do not feel that using functions will be needed.

// Begin main
int main()
{

	string filename, oldFile; // initialize filename, oldfile, newfile
	ofstream fileOutput, oldOutput; // output for above
	ifstream fileInput, oldInput; // input for above

	int var1, var2, var3; // open up some space for variables from the data we're getting from the file
	char yesNo;

	// cout << "Please enter the filename of the file you would like this program to read (Type HELP for file formatting): "; // prompt user
	cout << "Please enter the filename of the file you would like this program to read: "; // prompt user
	cin >> filename;

	// None of this code works. :(

	/*
	if (filename.c_str == "HELP") {
	
		cout << "\nThe contents of the data files this program uses goes as follows:";
		cout << "\nThree integers, one on each line starting at the topmost line.\n\n";

		cout << "Please enter the filename of the file you would like this program to read (Type HELP for file formatting): "; // prompt user
		cin >> filename;

		if (filename.c_str == "HELP") {
		
			cout << "\n\nWe already showed you some help you silly. :)" << endl;
			exit(2);

		}

	}*/
	

	fileInput.open(filename.c_str());

	// check if we can reach the file
	// where to put a file can be a bit confusing at first, I eventually figured it out
	if (fileInput.fail()) {

		cout << "This file does not exist! Or we just don't have access to it." << endl;
		cout << "Please supply a valid filename next time!" << endl;

		// i also could've had the user make a new file with some variables
		// but i really want to sleep at a reasonable hour

		exit(1);

	}

	// read from the file

	fileInput >> var1 >> var2 >> var3; // take the three integers out of the file
	// testing, making sure i did this right
	//cout << var1 << endl << var2 << endl << var3;

	// print the contents of the file to the user, so we could confirm 

	cout << "The values contained in the file are: " << var1 << ", " << var2 << ", and " << var3 << "." << endl;

	// prompt the user if we'd like to save the current file's values to a different file before continuing
	// if yes, output the given files to a new file at the user given location

	cout << "Would you like to back these values up (or write to) a different file? (y/n): ";
	cin >> yesNo;

	//this switch statement looks ugly
	switch (yesNo) {
		case 'y':{
			cout << "\nPlease enter a filename to write the old data to: ";
			cin >> oldFile;

			oldOutput.open(oldFile.c_str());

			cout << endl << "Writing..." << endl;

			// Attempt to write to the location
			oldOutput << var1 << endl << var2 << endl << var3;

			// check if the file was actually written
			if (oldOutput.fail()) {
			
				cout << endl << "Couldn't open file!" << endl;
				cout << "Could it be in a read-only directory?" << endl;
				exit(1);
			}

			cout << "Writing OK!";

			break;
		}
		case 'n': {
			cout << "\nIf you say so!\n";
			break;
		}

	}


	// Do some hardcoded calculations and write them to the file we opened and read from.

	cout << endl << "We will now add half of the variable's value to the variable." << endl; 

	var1 = var1 + (var1 / 2); // These should be pretty straight forward.
	var2 = var2 + (var2 / 2);
	var3 = var3 + (var3 / 2);

	// print the results of the above
	cout << "The results being: " << var1 << ", " << var2 << ", and " << var3 << "." << endl;

	cout << "Would you like to write these to the file we opened? (y/n): ";
	cin >> yesNo;

	switch (yesNo) { // blatant copypaste of a previous switch statement
		case 'y': {
			fileOutput.open(filename.c_str());

			cout << endl << "Writing..." << endl;

			// Attempt to write to the location
			fileOutput << var1 << endl << var2 << endl << var3;

			// check if the file was actually written
			if (fileOutput.fail()) {

				cout << endl << "Couldn't open file!" << endl;
				cout << "Could it be in a read-only directory?" << endl;
				exit(1);
			}
		
			cout << "Writing OK!";

			break;
		}
		case 'n': {
			cout << "\nIf you say so!\n";
			break;
		}

	}

	cout << endl; // spacing
    return 0; // end program

}