 /* 
  * 
  * Solution to Chapter 3, Page 93, # 13 (A&B)
  * In: A First Book Of C++, 4th Edition
  * Written By: Gregory McGuire
  * 
  * Twitter: @gm_mcg
  * Gitlab: gmcg
  * 
  * This program was made for a specific purpose.
  * Said purpose is to calculate 2.36 miles into the equivalent in feet.
  * This can easily be modified to accept user input.
  * 
  */

#include <iostream>

using namespace std;

int main()
{
	
	double tripDistFeet; // The distance of the trip in feet.
	double tripDistMi = 2.36; // The distance of the trip in miles.
	
	// cin >> tripDist;
	
	cout << "The trip's length is " << tripDistMi << " miles." << endl; // Print what we know to the user.
	
	// I could've used a constant here but chose not to.
	tripDistFeet = (tripDistMi * 5280); // Miles -> Feet
	
	cout << tripDistMi << " miles comes out to around " << tripDistFeet << " feet." << endl;
	
	return 0;
}