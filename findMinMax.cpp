//
//	Solution to Chapter 7, Page 301, Problem 8 (All Parts)
//	In: A First Book of C++, 4th Edition
//	
//	Written by Gregory McGuire on April 5th, 2018 (Finished April 9th, 2018)
//	Twitter: @gm_mcg
//	web: https://gregory.red (WIP)
//	gitlab: gmcg
//	


// begin includes
#include <iostream> // input/output


using namespace std; // important, we're using the standard libraries


// begin main
int main()
{
	// init variables
	int fmax[10];
	int i; // counter variable
	int arrayPlacementMax, arrayPlacementMin; // keeping track of things

	// collect the first value outside of the loop, 
	// as we don't have anything to compare it to yet
	cout << "Something to keep in mind, arrays start at zero!" << endl << endl; // notify the user about the importance of arrays
	cout << "Please enter a (whole) number: "; // prompt user
	cin >> fmax[0]; // collect input
	cout << endl; // print a new line

	// init arrayPlacement vars
	arrayPlacementMax = 0;
	arrayPlacementMin = 0;

	// notify the user that the value we just took is element 0, not element 1
	cout << "The value you just inputted is considered to be element " << arrayPlacementMax << "." << endl << endl;

	// check if the number isn't a letter/invalid input
	if (fmax[0] == -858993460 || fmax[0] == 858993460) {
	
		cout << "Try entering a number instead next time. :)" << endl;
		return -1; // this is an error btw

	}

	// begin loop
	for (i = 1; i <= 9; ++i) {
	
		cout << "Enter another (whole) number: "; // prompt the user for another number
		cin >> fmax[i]; // collect input
		cout << endl; // print a new line

		// copypaste a previous check to the loop
		if (fmax[i] == -858993460 || fmax[i] == 858993460) {

			cout << "Try entering a number instead next time. :)" << endl;
			// i would have the user re-enter the value but i don't think it is needed for a homework assignment
			return -1; // this is an error btw

		}

		//arrayPlacementMax2 = i; // store the current value of i

		if (fmax[i] < fmax[arrayPlacementMax]) {
		
			cout << fmax[i] << " is less than " << fmax[arrayPlacementMax] << "." << endl;
			
			// check if the value is lower than the previously recorded lower number
			if(fmax[i] < fmax[arrayPlacementMin]){
				
					// then assign i to arrayPlacementMin if true
					arrayPlacementMin = i;
				
				}
			
			//cout << fmax[arrayPlacementMin] << " is currently the lowest number given" << endl; // redundant given the final output
			cout << fmax[i] << " is element number " << i << "." << endl;
		
		}
		else if (fmax[i] > fmax[arrayPlacementMax]) {
		
			cout << fmax[i] << " is greater than " << fmax[arrayPlacementMax] << "." << endl;
			cout << fmax[i] << " is element number " << i << "." << endl << endl;
			arrayPlacementMax = i; // pass the value of i to arrayPlacementMax, as it is the larger value

		}
		else {
		
			cout << fmax[i] << " is equal to " << fmax[arrayPlacementMax] << "." << endl;
			cout << fmax[i] << " is element number " << i << "." << endl << endl;
			arrayPlacementMax = i; // for the sake of clarity, update arrayPlacementMax to the current value of i.

		}

	}

	// print results
	cout << "The largest number you have given is " << fmax[arrayPlacementMax] << " and the lowest given number is " << fmax[arrayPlacementMin] << "." << endl;

	cout << endl; // spacing
    return 0; // end program

}

// in hindsight i probably could've used functions in this
