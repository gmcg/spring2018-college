A bunch of programs I wrote for homework during Spring 2018 for my C++ Programming class at Quincy College.
These will not be maintained, and they may vary in quality. Some could've been done a lot better, hell I might even try to improve some in the future.

Please don't turn these in as your own works, my professor at least will be made known of this repo before it goes public. ;)

Book: A First Book of C++, 4th Edition