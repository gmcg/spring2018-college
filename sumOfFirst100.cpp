
// does exactly what it says on the tin.

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{

	int sum;
	int n = 100, // number of terms to be added
		a = 1, // the first number
		d = 1; // the difference between two numbers

	cout << "This program will calculate the sum of the first 100 numbers." << endl;

	sum = (n / 2) *(2 * a + (n - 1) * d); // literally just lifting the equation out of the book as it just works

	cout << endl << "The sum of the first 100 numbers is: " << sum << endl;

    return 0;

}