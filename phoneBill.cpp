// 
// Midterm for C++ Programming
// 
// This program will calculate and print a phone bill.
// The provider provides a premium and regular service.
// Rates vary and are computed as follows.
//
// Reg: initCostReg is $10, first 50 minutes are free, each minute after that is $0.20 more.
// Prem: initCostPrem is $25, calls are unlimited, so minutes aren't part of the problem.
//
// User Input: Account number, number of minutes used, service code.
// Program Output: Same as above, along with the amount due.
//
// ___________________________________________
// Solution written by: Gregory M. McGuire, On March 20th, 2018.
// Twitter: @gm_mcg
// Web: https://tale.rip
// -------------------------------------------
//

// Program init

#include <iostream> // input/output
#include <iomanip> // manipulation

using namespace std; // actually important


// begin main

int main()
{

	// init variables

	int minutesUsedGM, // total minutes used
		userAccNumberGM, // user account number
		regMinuteOverage; // the amount of minutes you've gone over

	double regInitCostGM = 10.00, // initial cost of the regular plan
		premInitCostGM = 25.00, // initial cost of the premium plan
		regMinuteOverageCostGM = 0.20, // how much each minute costs after you reach 50 minutes
		totalBillGM; // the amount due

	char svcCodeGM; // Service Cod; could be R, r, P, or p


	// insert cheesy fictional phone provider name here

	cout << "Avalon Telecommunications: Billing" << endl << "Please enter your account number: ";
	cin >> userAccNumberGM; // collect the account number from the user, should be a whole non-zero positive number
	cout << endl;

	// debugging: print the input we just recieved.
	// cout << userAccNumberGM;

	// check input

	if (userAccNumberGM <= 0)
	{
		cout << endl;
		cout << "!! Invalid account number. !!" << endl;
		return -1; // this is an error, closes the program
	}

	// collect the number of minutes used from the user
	cout << "Please enter the number of minutes used: ";
	cin >> minutesUsedGM;
	cout << endl;

	if (minutesUsedGM <= 0)
	{
		cout << endl;
		cout << "!! Invalid input. The number of minutes used must be a positive number! !!" << endl;
		return -1; // this is an error, closes the program
	}

	cout << "Please enter the service code for your plan (R is Regular, P is Premium): ";
	cin >> svcCodeGM;
	cout << endl;

	// input correction for the switch statement
	// probably won't be needed
	// don't actually know if switch statement input is case sensitive

	if (svcCodeGM == 'r' || svcCodeGM == 'R') {
	
		svcCodeGM = 'R';

	}
	else if (svcCodeGM == 'p' || svcCodeGM == 'P') {
	
		svcCodeGM = 'P';
	
	}
	else {

		// cout << "can't you read?";
		cout << endl;
		cout << "!! Invalid input. !!" << endl;
		return -1; // this is an error, closes the program

	}

	// debugging: print out the input we just recieved
	//cout << svcCodeGM << endl << endl;

	// begin switch statement

	switch (svcCodeGM)
	{
	case 'R':
		cout << endl << "Account number: " << userAccNumberGM;
		cout << endl << "Number of Minutes Used: " << minutesUsedGM;
		cout << endl << "You are on the Regular plan. As a result the first 50 minutes are free." << endl;

		if (minutesUsedGM <= 50) {
			totalBillGM = regInitCostGM;
		}
		else {
			regMinuteOverage = minutesUsedGM - 50;
			totalBillGM = regInitCostGM + (regMinuteOverage * regMinuteOverageCostGM);
		}
		cout << endl << "Total amount due: $" << fixed << setprecision(2) << totalBillGM;
		break;

	case 'P':
		cout << endl << "Account number: " << userAccNumberGM;
		cout << endl << "Number of Minutes Used: " << minutesUsedGM;
		cout << endl << "You are on the Premium plan. The cost of extra minutes is included in the monthly fee." << endl;

		totalBillGM = premInitCostGM;

		cout << endl << "Total amount due: $" << fixed << setprecision(2) << totalBillGM << endl;
		break;

	default:
		break;
	}

	cout << endl; // spacing

    return 0; // end program
}