// ch2-pg69-num13.cpp

#include <iostream>

using namespace std;

int main()
{

	// we're using floats here :v

	float firstNum = 105.62, // The first number.
		secNum = 89.352, // The second number.
		thirdNum = 98.67; // The third number.

	float total; // Total sum of all the above numbers.
	float average; // The average will be calculated and passed to this variable.

	total = firstNum + secNum + thirdNum;

	average = total / 3.0;

	cout << "The three numbers" << endl <<  // print the three hardcoded numbers for the sake of the user
		"105.62, 89.352, 98.67" << endl << 
		"Total sum of the three numbers: " << total << endl <<  // print the total sum
		"The average of the three numbers: " << average << endl; // print the average

    return 0;

}