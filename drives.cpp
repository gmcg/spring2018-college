// ch4-pg172-num3
// switch statement homework :v

#include <iostream>
//#include <iomanip> // turns out I didn't really need this

using namespace std;

int main()
{
	int userSelection;
	
	cout << "Please enter any number 1-4: ";
	cin >> userSelection;
	
	switch (userSelection)
	{
		case 1:
			cout << endl /* just to put this on a new line */ << "The capacity of that storage drive type is: 2 Gigabytes." << endl;
			break;
		case 2:
			cout << endl << "The capacity of that storage drive type is: 4 Gigabytes." << endl;
			break;
		case 3:
			cout << endl << "The capacity of that storage drive type is: 16 Gigabytes." << endl;
			break;
		case 4:
			cout << endl << "The capacity of that storage drive type is: 4 Gigabytes." << endl; 
			break;
		default:
			cout << endl << "Unknown drive number." << endl;
			return -1; // this is an error ;_>'
	}
	return 0;
}