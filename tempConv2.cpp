//
// A FIRST BOOK OF C++, FOURTH EDITION
// 
// STUDENT WRITTEN SOLUTION FOR: CH 5, PAGE 199, #8
// 
// PROGRAM WRITTEN BY GREGORY M. MCGUIRE
// STARTED: MARCH 5TH, 2018
// 
// EQUATIONS:
// fahrenheit = (9.0/5.0)* celsius + 32.0
//

#include <iostream> // taking input and printing output
#include <iomanip> // formatting output

using namespace std;

int main()
{
	double celsius, fahrenheit, celInc; // celInc, the amount to increase Celsius by before each consequent conversion
	int conversions,  // number of conversions to make
	i; // counter variable
	
	// write now format later
	
	// make things a bit more readable in the IDE by just spacing it out like this
	cout << endl; // start things with a new line
	cout << "For this program, we will ask you to provide three things." << endl;
	cout << "A starting Celsius value." << endl;
	cout << "The number of conversions to be made. (As a positive, whole, number.)" << endl;
	cout << "And lastly, the amount to increment by for each conversion. (As a non-zero value.)" << endl << endl; // another endl just to space things out a bit more.
	
	// prompt the user for the values requested above
	cout << "Please enter the information requested." << endl;
	//cin >> celsius, conversions, celInc; // this didn't work
	cout << "A temperature in Celsius: ";	cin >> celsius;		cout << endl; // group things together
	cout << "The number of conversions to make (Any whole number >0): ";		cin >> conversions;		cout << endl;
	cout << "An amount to increment by (Any number !=0, will be cut off at 2 decimal places): ";		cin >> celInc;		cout << endl;

	// I would add some checks for letters or unexpected input but as this isn't being used for serious production it doesn't seem 
	// to be needed unless the problem asks for it.
	
	// check if things == 0
	if(celInc == 0 || conversions == 0){
		
		cout << "It seems you entered a value of zero for either your increment or number of conversions," << endl // more spacing
		<< "as a result this will only make C->F convert once." << endl << endl; // two endls to space things out a bit more
		
		cout << "   CELSIUS           FAHRENHEIT   " << endl;
		cout << "----------------------------------" << endl;
		
		fahrenheit = (9.0/5.0)* celsius + 32.0; // do the conversion
		
		cout << setw(5) << fixed << setprecision(2) << " " << celsius << "           " << setw(10) << fixed << setprecision(2) << fahrenheit << endl; // print out the things
		
		return 0; // this program finished properly, and should close after this
		
		}
	
	// print labels
	cout << "   CELSIUS           FAHRENHEIT   " << endl;
	cout << "----------------------------------" << endl;
	
	// begin for loop
	for(i = 0; i < conversions; ++i){
		
			fahrenheit = (9.0/5.0)* celsius + 32.0; // do conversion
			cout << setw(5) << fixed << setprecision(2) << " " << celsius << "           " << setw(10) << fixed << setprecision(2) << fahrenheit << endl; // print results
			celsius = celsius + celInc; // add the increment to the variable celsius
		
		}
	
	cout << endl; // another endl
	
	return 0;
	
}