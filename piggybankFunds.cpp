//
// Solution to Chapter 6, Page 255, Problem #7 A&B
// In: A First Book of C++, Fourth Edition
// 
// Written by: Gregory McGuire
//
// Twitter: @gm_mcg
// Web: https://gregory.red
//

// solution to problem 7a is at the bottom of this source file

// include some things
#include <iostream> // take input and print output
#include <iomanip> // manipulate output, etc

using namespace std; // important

// constants
const double QUARTER_VALUE = 0.25;
const double DIME_VALUE = 0.10;
const double NICKEL_VALUE = 0.05;
const double PENNY_VALUE = 0.01;

// function prototype(s)
double totamt(int quarters, int dimes, int nickels, int pennies); // returns a double to main


// begin main

int main()
{
	
	int totQuarters, totDimes, totNickels, totPennies; // you can't have a portion of a coin :(
	double finalAmount; // the result of the function gets passed back to this value
	
	// debugging...
	//totDimes = 3; // $0.30
	//totQuarters = 2; // $0.50
	//totNickels = 4; // $0.20
	//totPennies = 0;
	// total would be exactly $1.00
	
	// prompt the user for the amount of each coin that they have on hand
	// it would be too impractical to count a large amount of coins by hand, so this program won't see any large scale use.
	
	cout << "How many Quarters do you have? > ";
	cin >> totQuarters; cout << endl;
	
	cout << "How many Dimes do you have? > ";
	cin >> totDimes; cout << endl;
	
	cout << "How many Nickels do you have? > ";
	cin >> totNickels; cout << endl;
	
	cout << "How many Pennies do you have? > ";
	cin >> totPennies; cout << endl;
	
	// pass values to function
	finalAmount = totamt(totQuarters, totDimes, totNickels, totPennies);
	
	// output result(s)
	cout << fixed << setprecision(2) << "The total value for all your change is: $" << finalAmount;
	
	cout << endl; // spacing
	return 0; // the program ends here
	
}

// begin function
// see also, problem 7a
// 
double totamt(int quarters, int dimes, int nickels, int pennies){
	
	// local variables for processing 
	double totValueQuarters, totValueDimes, totValueNickels, totValuePennies; // holds the total value for the amount of X passed to the function
	double totValueTotal;
	
	// calculations...
	// should be easy to understand without comments, i'll let the var names speak for themselves
	totValueQuarters = quarters * QUARTER_VALUE;
	totValueDimes = dimes * DIME_VALUE;
	totValueNickels = nickels * NICKEL_VALUE;
	totValuePennies = pennies * PENNY_VALUE;
	
	// calculate the total value of the change
	totValueTotal = totValueDimes + totValueQuarters + totValueNickels + totValuePennies;
	
	// return the total value to main
	return totValueTotal;
	
	}