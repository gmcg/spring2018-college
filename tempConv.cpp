/*
 * 
 * Solution to Chapter 4, Page 165, #9
 * In: A First Book of C++, 4th Edition
 * Written By: Gregory McGuire
 * 
 * Twitter: @gm_mcg
 * Gitlab: gmcg
 * 
 */


// it converts temperature. todo: kelvin

#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	double inputTemperature, // User input.
			outputTemperature; // Output/Result
	char tempSym; // User input
	
	
	cout << "Please enter the temperature (EX: 40 F, 10 C) then press ENTER: ";
	cin >> inputTemperature >> tempSym;
	cout << endl; // spacing
	//cout << inputTemperature << tempSym; //testing
	
	if (tempSym == 'C' || tempSym == 'c'){
		
			outputTemperature = (9.0/5.0) * inputTemperature + 32.0;
			cout << inputTemperature << " degrees Celcius comes out to around " << fixed << setprecision(2) << outputTemperature << " degrees Fahrenheit." << endl;
			return 0;
			
		}
	else if (tempSym == 'F' || tempSym == 'f'){
		
			outputTemperature = (5.0/9.0) * (inputTemperature - 32.0);
			cout << inputTemperature << " degrees Fahrenheit comes out to around " << fixed << setprecision(2) << outputTemperature << " degrees Celcius." << endl;
			return 0;
		
		}
	else{
		
			cout << "This line should never appear. Unless you've entered a letter or something." << endl;
			return -1;
		
		}
		
	return 0;
}